package com.pimlada.midterm;

public class InfoGame {
    private String name;
    private int level;
    private double percentLevelUp;
    private int money;
    private int bell;

    public InfoGame(String name, int level, double percentLevelUp, int money, int bell) {
        this.name = name;
        this.level = level;
        this.percentLevelUp = percentLevelUp;
        this.money = money;
        this.bell = bell;
    }

    public void printInfoGame() {
        System.out.println("Info of TinyFarm Game");
        System.out.println("name : " + name);
        System.out.println("Level : " + level);
        System.out.println("Percent Level Up : " + percentLevelUp + " %");
        System.out.println("Money : " + money + " Baht");
        System.out.println("Bell : " + bell);
    }

    public String getName() {
        return name;
    }
    public int getLevel() {
        return level;
    }
    public double getPercentLevelUp() {
        return percentLevelUp;
    }
    public int getMoney() {
        return money;
    }
    public int getBell() {
        return bell;
    }
}
