package com.pimlada.midterm;

public class Animals {
    private String typeAnimal;
    private int amountAnimal;
    private int priceAnimal;

    public Animals(String typeAnimal, int amountAnimal, int priceAnimal) {
        this.typeAnimal = typeAnimal;
        this.amountAnimal = amountAnimal;
        this.priceAnimal = priceAnimal;
    }

    public void printAnimals() {
        System.out.println("Info of Animals");
        System.out.println("Type : " + typeAnimal);
        System.out.println("Amount : " + amountAnimal);
        System.out.println("Selling price (1): " + priceAnimal + " Baht");
    }

    public String getTypeAnimal() {
        return typeAnimal;
    }
    public int getAmountAnimal() {
        return amountAnimal;
    }
    public int getPriceAnimal() {
        return priceAnimal;
    }
}
