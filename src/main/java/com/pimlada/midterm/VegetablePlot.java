package com.pimlada.midterm;

public class VegetablePlot {
    private String typeVegetable;
    private int amountPlot;
    private int priceVegetable;

    public VegetablePlot(String typeVegetable, int amountPlot, int priceVegetable) {
        this.typeVegetable = typeVegetable;
        this.amountPlot = amountPlot;
        this.priceVegetable = priceVegetable;
    }
    public void printVegetablePlot() {
        System.out.println("Info of Vegetable Plot");
        System.out.println("Type : " + typeVegetable);
        System.out.println("Amount : " + amountPlot);
        System.out.println("Selling price (1/tree) : " + priceVegetable + " Baht");
    }

    public String getTypeVegetable() {
        return typeVegetable;
    }
    public int amountPlot() {
        return amountPlot;
    }
    public int priceVegetable() {
        return priceVegetable;
    }
}
