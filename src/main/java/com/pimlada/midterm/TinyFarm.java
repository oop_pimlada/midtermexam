package com.pimlada.midterm;

public class TinyFarm {
    public static void main(String[] args) {
        InfoGame info = new InfoGame("fxthemoon", 11, 82.82, 104688, 57);
        info.printInfoGame();
        System.out.println("------------------------");

        Animals animal1 = new Animals("Black Dairy Cow", 3, 1410);
        Animals animal2 = new Animals("White Sheep", 3, 575);
        Animals animal3 = new Animals("Black Sheep", 1, 920);
        Animals animal4 = new Animals("White Chicken", 3, 490);
        Animals animal5 = new Animals("Pink Pig", 2, 1090);
        animal1.printAnimals();
        animal2.printAnimals();
        animal3.printAnimals();
        animal4.printAnimals();
        animal5.printAnimals();
        System.out.println("------------------------");
        
        VegetablePlot vegetable1 = new VegetablePlot("Alfalfa", 3, 6);
        VegetablePlot vegetable2 = new VegetablePlot("Cotton", 3, 910);
        VegetablePlot vegetable3 = new VegetablePlot("Wheat", 4, 286);
        VegetablePlot vegetable4 = new VegetablePlot("Ginseng", 4, 812);
        vegetable1.printVegetablePlot();
        vegetable2.printVegetablePlot();
        vegetable3.printVegetablePlot();
        vegetable4.printVegetablePlot();

    }
}
